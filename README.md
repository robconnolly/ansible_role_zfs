Ansible ZFS Role
=========

An Ansible role to install and configure ZFS

Requirements
------------

None

Role Variables
--------------

The following variables are available:

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: zfs
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
